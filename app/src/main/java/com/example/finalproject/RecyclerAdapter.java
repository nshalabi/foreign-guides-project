package com.example.finalproject;

import android.graphics.drawable.Drawable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;
import java.util.ArrayList;
import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;


public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder>{
    private Listener listener;
    private ArrayList<Integer> imgIds;
    private ArrayList<String> name;
    private ArrayList<String> description;
    private ArrayList<String> date;

    public RecyclerAdapter(ArrayList<String> name, ArrayList<Integer> imgIds,ArrayList<String> description, ArrayList<String> date){
        this.name = name;
        this.imgIds = imgIds;
        this.description = description;
        this.date = date;

    }

    @Override
    public int getItemCount(){
        try {
            return name.size();
        } catch(Exception e){
            return  0;
        }
    }//end of getItemCount

    @NonNull
    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder
            (ViewGroup parent, int viewType){

        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).inflate(R.layout.events_historical_image, parent, false);
        return new ViewHolder(cv);
    }//end of onCreateViewHolder

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position){
        CardView cardView = holder.cardView;
        ImageView imageView = (ImageView) cardView.findViewById(R.id.events_image);
        Log.i("real image", " is: " + R.drawable.acro);
        Log.i("database image", "is: " + imgIds.get(position));
       // Log.i("2nd database image", "is: " + imgIds.get(position));
        Log.i("2nd real image", " is: " + R.drawable.bootcamp);

        Drawable drawable = ContextCompat.getDrawable(cardView.getContext(),imgIds.get(position));
        imageView.setImageDrawable(drawable);

        TextView tv1 = (TextView) cardView.findViewById(R.id.description);
        tv1.setText("DESCRIPTION: " + description.get(position) + "\n");
        TextView tv2 = (TextView) cardView.findViewById(R.id.date);
        tv2.setText("DATE: " + date.get(position) + "" + "\n");
        TextView tv3 = (TextView) cardView.findViewById(R.id.name);
        tv3.setText("NAME: " + name.get(position) + "" + "\n");


        //attach a click listener
        cardView.setOnClickListener(new View.OnClickListener(){
            @Override
            public void onClick(View view){
                if(listener != null){
                    listener.onClick(position, view);
                }
            }
        });


    }//end of onBlindViewHolder



    public  static class ViewHolder extends RecyclerView.ViewHolder{

        private CardView cardView;
        public ViewHolder(CardView v){
            super(v);
            cardView = v;
        }//end of ViewHolder const.

    }//end of ViewHolder inner class

    interface Listener{
        void onClick(int position, View view);
    }

    public void setListener(Listener listener){
        this.listener = listener;
    }

}//end of adapter


