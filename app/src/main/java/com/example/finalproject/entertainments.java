package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;

import android.content.Intent;
import android.os.Bundle;
import android.view.View;

public class entertainments extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_entertainments);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
    }// end onCreate

    public void playSongs(View view){
        Intent intent = new Intent(this, songs.class);
        startActivity(intent);


    }
    public void playVideo(View view){
        Intent intent = new Intent(this, video.class);
        startActivity(intent);

    }

    public void readNOLA(View view){
        Intent intent = new Intent(this, readNola.class);
        startActivity(intent);

    }

}//end class entertainment.
