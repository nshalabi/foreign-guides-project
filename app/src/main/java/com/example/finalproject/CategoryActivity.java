package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.fragment.app.FragmentTransaction;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;
import android.widget.TextView;
import android.widget.Toast;

public class CategoryActivity extends AppCompatActivity {

    public static final String CATEGORY_ID = "category";
    private String[] captions;
    private int[] imgIDs;
    private static int category;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_category);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        TextView title = (TextView) findViewById(R.id.categoryTitle);

        if(getIntent().getExtras() != null) {
            category = (Integer) getIntent().getExtras().getInt(CATEGORY_ID);
        }

        boolean dataExists = false;

        if(category == 1) { //hotels
            title.setText("Hotels");
            dataExists = getDataFromDatabase("HOTEL");
        }
        else if (category == 2) { // restaurants
            title.setText("Restaurants");
            dataExists = getDataFromDatabase("RESTAURANT");
        }

        if(dataExists) {
            RecyclerView recyclerView = (RecyclerView) findViewById(R.id.categoryRecycler);

            CaptionedImg adapter = new CaptionedImg(captions, imgIDs);
            recyclerView.setAdapter(adapter);
            recyclerView.setLayoutManager(new LinearLayoutManager(this));

            adapter.setListener(new CaptionedImg.Listener() {
                @Override
                public void onClick(int position) {
                    if (category == 1) { //hotels
                        Intent intent = new Intent(
                                CategoryActivity.this, HotelDetailActivity.class);
                        intent.putExtra(CategoryActivity.CATEGORY_ID, 1);
                        intent.putExtra(HotelDetailActivity.HOTEL_ID, position);
                        startActivity(intent);
                    } else if (category == 2) { // restaurants
                        Intent intent = new Intent(
                                CategoryActivity.this, RestaurantDetailActivity.class);
                        intent.putExtra(CategoryActivity.CATEGORY_ID, 2);
                        intent.putExtra(RestaurantDetailActivity.RESTAURANT_ID, position);
                        startActivity(intent);
                    }
                }
            });
        }

    }

    public boolean getDataFromDatabase(String tableName) {
        SQLiteOpenHelper dbHelper = new DBHelper(this);
        try {
            SQLiteDatabase readDB = dbHelper.getReadableDatabase();
            Cursor cursor = readDB.query(tableName,
                    new String[]{"_id", "NAME", "IMAGE_ID"}, null,
                    null, null, null, null);

            if(!cursor.moveToFirst()) return false;

            captions = new String[cursor.getCount()];
            imgIDs = new int[cursor.getCount()];
            int i=0;
            do {
                captions[i] = cursor.getString(1);
                imgIDs[i] = cursor.getInt(2);
                i++;
            }
            while(cursor.moveToNext());

            cursor.close();
            readDB.close();
            return true;
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this,
                    "Database Unavailable!", Toast.LENGTH_LONG);
            toast.show();
            return false;
        }
    }

    @Override
    protected void onSaveInstanceState(Bundle savedInstanceState) {
        super.onSaveInstanceState(savedInstanceState);
        savedInstanceState.putInt(CATEGORY_ID, category);
    }
}
