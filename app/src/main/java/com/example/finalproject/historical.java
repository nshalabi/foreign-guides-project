package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.finalproject.myDatabase.insertHistorical;

public class historical extends AppCompatActivity {
    ArrayList<String> names = new ArrayList<>();
    ArrayList<String> since = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();
    ArrayList<Integer> imgId = new ArrayList<>();
    ArrayList<String> addresses = new ArrayList<>();
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_historical);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        RecyclerView recycler = (RecyclerView) findViewById(R.id.historical_recycler);

        SQLiteOpenHelper databaseHelper = new myDatabase(this);
        try{
            SQLiteDatabase db = databaseHelper.getReadableDatabase();
//            insertHistorical(db, "Chalmette battlefield.", 1815, "The Chalmette Battlefield commemorates two significant wars in American history, both having major implications on our country's history.", R.drawable.battelefield1 , "8606 W St Bernard Hwy, Chalmette, LA 70043");
//            insertHistorical(db, "Basin street station.", 1897, "This is a natural starting point for visitors coming to town. Take a walk to the building's rooftop for a great view of the city.", R.drawable.basin1 , "501 Basin St, New Orleans, LA 70112");
//            insertHistorical(db, "New Orleans Streetcars.", 1851, "No visit to New Orleans is complete without a ride on the streetcar. Single day, three-day or monthly Jazzy passes are available online at http://www.norta.com or in cash from the streetcar conductor.\n", R.drawable.streetcars1 , "NULL");
//            insertHistorical(db, "Jackson Square.", 1815, " Jackson Square is one of the iconic places in the city. : Jackson Square is one of the iconic places in the city.", R.drawable.jackson1, "Jackson AveNew Orleans, LA");
//            insertHistorical(db, "Historic New Orleans Collection Museum", 1966, "The collection was founded in 1966 to preserve the history and culture of New Orleans and the Gulf South. Located in a historic complex of French Quarter buildings.", R.drawable.neworleanscollection1 , "533 Royal St, New Orleans, LA 70130");
//            insertHistorical(db, "St Louis Cathedral", 1700, "It looks like a fairy tale castle, but in fact, St. Louis Cathedral is the oldest continuously operating cathedral in the US. Originally built in the late 1700.", R.drawable.stlouiscathedral1, "615 Pere Antoine Alley, New Orleans, LA 70116" );


            Cursor cursor = db.query("HISTORICAL", new String[]{"_id", "NAME", "SINCE", "DESCRIPTION", "IMAGE_RESOURCE_ID", "ADDRESS"}, null, null, null, null, null);

            if(cursor.moveToFirst()) {

                do {
                    names.add(cursor.getString(1));
                    since.add(cursor.getString(2));
                    description.add(cursor.getString(3));
                    imgId.add(cursor.getInt(4));
                    addresses.add(cursor.getString(5));
                } while (cursor.moveToNext());
            }
            else{
                Log.d("test", "apparently there wasn't a 'first' to move the cursor to!");
            }
            
            cursor.close();
            db.close();
        } catch(SQLiteException e){
            Toast toast = Toast.makeText(this, "database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }//end of catch block

        RecyclerAdapter adapter = new RecyclerAdapter(names, imgId, description, since);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager( new LinearLayoutManager(this));
        adapter.setListener(new RecyclerAdapter.Listener() {
            @Override
            public void onClick(int position, View view) {
                String location = addresses.get(position);
                if(!location.equals("NULL")) {
                    String map = "http://maps.google.co.in/maps?q=" + location;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                    startActivity(intent);
                }else{
                    Toast toast = Toast.makeText(view.getContext(), "unavailable address right now", Toast.LENGTH_SHORT);
                    toast.show();
                }
        }
    });//end of adapter.setListner.

    }//end of onCreate

}//end of class
