package com.example.finalproject;


import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class SigninFragment extends Fragment implements View.OnClickListener{

    private View layout;

    public SigninFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_signin, container, false);
        Button btnGo = (Button) layout.findViewById(R.id.btnGo);
        btnGo.setOnClickListener(this);
        TextView btnSignUp = (TextView) layout.findViewById(R.id.noacc);
        btnSignUp.setOnClickListener(this);

        return layout;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnGo) {

            String username = ((EditText) layout.findViewById(
                    R.id.oldUser)).getText().toString();
            String password = ((EditText) layout.findViewById(
                    R.id.oldpass)).getText().toString();

            SQLiteOpenHelper dbHelper = new DBHelper(getActivity());
            try {
                SQLiteDatabase readDB = dbHelper.getReadableDatabase();
                Cursor cursor = readDB.query("PERSON", new String[]{"_id", "USERNAME", "PASSWORD"},
                        "USERNAME = ?",
                        new String[]{username},
                        null, null, null);

                if(cursor.getCount() == 1) {
                    cursor.moveToFirst();
                    if(cursor.getString(2).equals(password)) {
                        Toast toast = Toast.makeText(getActivity(),
                                "Sign In successful!", Toast.LENGTH_SHORT);
                        toast.show();
                        //go to homepage
                        Intent intent = new Intent(getActivity(), MainActivity.class);
                        startActivity(intent);
                    }
                    else {
                        Toast toast = Toast.makeText(getActivity(),
                                "Sign In failed! Try again.", Toast.LENGTH_LONG);
                        toast.show();
                    }
                }
                else {
                    Toast toast = Toast.makeText(getActivity(),
                            "Sign In failed! Try again.", Toast.LENGTH_LONG);
                    toast.show();
                }
                cursor.close();
                readDB.close();
            } catch (SQLiteException e) {
                Toast toast = Toast.makeText(getActivity(),
                        "Sign In failed!", Toast.LENGTH_LONG);
                toast.show();
            }
        }
        else if(v.getId() == R.id.noacc) {

            FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
            ft.replace(R.id.fragContainer, new SignUpFragment());
            ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
            ft.addToBackStack(null);
            ft.commit();
        }
    }
}
