package com.example.finalproject;


import android.content.ContentValues;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.os.AsyncTask;
import android.os.Bundle;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentTransaction;

import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;


/**
 * A simple {@link Fragment} subclass.
 */
public class SignUpFragment extends Fragment implements View.OnClickListener{

    private View layout;

    public SignUpFragment() {
        // Required empty public constructor
    }


    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        layout = inflater.inflate(R.layout.fragment_sign_up, container, false);
        Button btnConfirmSignUp = (Button) layout.findViewById(R.id.btnConfirmSignUp);
        btnConfirmSignUp.setOnClickListener(this);

        return layout;
    }

    @Override
    public void onClick(View v) {
        if(v.getId() == R.id.btnConfirmSignUp) {
            String user = ((EditText)layout.findViewById(R.id.newUser)).getText().toString();
            new UpdateSignUpTask().execute(user);
        }
    }

    private class UpdateSignUpTask extends AsyncTask<String,Void,Boolean> {

        private ContentValues signUpValues;

        protected void onPreExecute() {
            String user = ((EditText)layout.findViewById(R.id.newUser)).getText().toString();
            String firstName = ((EditText)layout.findViewById(R.id.firstName)).getText().toString();
            String lastName = ((EditText)layout.findViewById(R.id.lastName)).getText().toString();
            String birth = ((EditText)layout.findViewById(R.id.dateOfBirth)).getText().toString();
            String email = ((EditText)layout.findViewById(R.id.email)).getText().toString();
            String pass = ((EditText)layout.findViewById(R.id.newPass)).getText().toString();

            if(user.isEmpty() || firstName.isEmpty() || lastName.isEmpty()
            || birth.isEmpty() || email.isEmpty() || pass.isEmpty()) {
                Toast toast = Toast.makeText(getActivity(),
                        "Make sure all fields are filled", Toast.LENGTH_LONG);
                toast.show();

            }else {
                signUpValues = new ContentValues();
                signUpValues.put("USERNAME", user);
                signUpValues.put("PASSWORD", pass);
                signUpValues.put("FIRSTNAME", firstName);
                signUpValues.put("LASTNAME", lastName);
                signUpValues.put("EMAIL", email);
                signUpValues.put("DATEOFBIRTH", birth);
            }
        }

        protected Boolean doInBackground(String... strings) {
            String username = strings[0];
            SQLiteOpenHelper dbHelper = new DBHelper(getActivity());
            try {
                SQLiteDatabase readDB = dbHelper.getReadableDatabase();
                Cursor cursor = readDB.query("PERSON", new String[]{"_id", "USERNAME"},
                        "USERNAME = ?",
                        new String[]{username},
                        null, null, null);
                if(cursor.moveToFirst()) { //username already exists
                    cursor.close();
                    readDB.close();
                    return false;
                }
                else {
                    if(signUpValues != null) {
                        SQLiteDatabase writeDB = dbHelper.getWritableDatabase();
                        writeDB.insert("PERSON", null, signUpValues);

                        writeDB.close();
                        return true;
                    } else return false;
                }
            } catch (SQLiteException e) {
                return false;
            }
        }

        protected void onPostExecute(Boolean success) {
            if(success) {
                FragmentTransaction ft = getActivity().getSupportFragmentManager().beginTransaction();
                ft.replace(R.id.fragContainer, new SigninFragment());
                ft.setTransition(FragmentTransaction.TRANSIT_FRAGMENT_FADE);
                ft.addToBackStack(null);
                ft.commit();
            }
            else {
                Toast toast = Toast.makeText(getActivity(),
                        "Sign Up failed!", Toast.LENGTH_LONG);
                toast.show();
            }
        }
    }
}
