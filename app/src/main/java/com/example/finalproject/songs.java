package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;
import java.util.ArrayList;

import static com.example.finalproject.myDatabase.insertSong;

public class songs extends AppCompatActivity {
    private ListView lv;
    ArrayList<String> songsNames;
    ArrayList<String> songslinks;
    @Override
    protected void onCreate(Bundle savedInstanceState){
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_songs);
        lv = (ListView) findViewById(R.id.lv);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        songsNames = new ArrayList<>();
        songslinks = new ArrayList<>();

        SQLiteOpenHelper databaseHelper = new myDatabase(this);
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();

            Cursor cursor = db.query("SONGS", new String[]{"_id", "SONGNAME", "LINK"}, null, null, null, null, null);
            if(cursor.moveToFirst()) {

                do {
                    songsNames.add(cursor.getString(1));
                    songslinks.add(cursor.getString(2));


                } while (cursor.moveToNext());
            }
            else{
                Log.d("test", "apparantly there wasn't a 'first' to move the cursor to!");
            }

            cursor.close();
            db.close();
        } catch(SQLiteException e){
            Toast toast = Toast.makeText(this, "database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }//end of catch block

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_expandable_list_item_1,
                songsNames );

        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0,View arg1, int position, long arg3)
            {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(songslinks.get(position)));
                startActivity(myIntent);

            }
        });
    }
}
