package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class RestaurantDetailActivity extends AppCompatActivity {

    public static final String RESTAURANT_ID = "restaurantID";
    private String website = "";
    private String address = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_restaurant_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        int restaurantId = (Integer) getIntent().getExtras().getInt(RESTAURANT_ID);

        SQLiteOpenHelper dbHelper = new DBHelper(this);

        try {
            SQLiteDatabase readDB = dbHelper.getReadableDatabase();
            Cursor cursor = readDB.query("RESTAURANT",
                    new String[]{"_id", "NAME", "DISH_IMAGE_ID", "ADDRESS",
                            "WEBSITE", "DISH_NAME", "DISH_DESC"},
                    "_id = ?",
                    new String[]{Integer.toString(restaurantId + 1)},
                    null, null, null);

            if(cursor.moveToFirst()) {

                TextView name = (TextView) findViewById(R.id.restaurantName);
                name.setText(cursor.getString(1));

                ImageView dishImg = (ImageView) findViewById(R.id.dishImg);
                dishImg.setImageResource(cursor.getInt(2));
                dishImg.setContentDescription(cursor.getString(5));

                TextView addr = (TextView) findViewById(R.id.restaurantAddr);
                addr.setText(cursor.getString(3));
                address = cursor.getString(3).toString();

                TextView web = (TextView) findViewById(R.id.restaurantWeb);
                SpannableString content = new SpannableString("Go to: " + cursor.getString(4).toString());
                content.setSpan(new UnderlineSpan(), 7, content.length(), 0);
                web.setText(content);
                //web.setText(cursor.getString(4));
                website = cursor.getString(4).toString();

                TextView dishName = (TextView) findViewById(R.id.dishName);
                dishName.setText(cursor.getString(5));

                TextView dishDesc = (TextView) findViewById(R.id.dishDesc);
                dishDesc.setText(cursor.getString(6));
            }

            cursor.close();
            readDB.close();
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this,
                    "Database Unavailable!", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void onRestaurantWebClick(View v) {
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+website));
        startActivity(webIntent);
    }

    public void onRestaurantAddrClick(View v) {
        Uri gmmIntentUri = Uri.parse("geo:0,0?q=" + Uri.encode(address));
        Intent mapIntent = new Intent(Intent.ACTION_VIEW, gmmIntentUri);
        mapIntent.setPackage("com.google.android.apps.maps");
        if(mapIntent.resolveActivity(getPackageManager()) != null) {
            startActivity(mapIntent);
        }
    }
}
