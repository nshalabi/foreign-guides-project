package com.example.finalproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.net.URL;


public class myDatabase extends SQLiteOpenHelper {

    private static final String DB_NAME = "database";
    private static final int DB_VERSION = 1;

    public myDatabase(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);


    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int i, int i1) {
        updateMyDatabase(db, i, i1);
    }


    protected static void insertEvent(SQLiteDatabase db,
                                      String name, String date, String desc,
                                      int resourceId, String address) {

        ContentValues eventValues = new ContentValues();
        eventValues.put("NAME", name);
        eventValues.put("IMAGE_RESOURCE_ID", resourceId);
        eventValues.put("Date_Time", date );
        eventValues.put("Description", desc);
        eventValues.put("ADDRESS", address );
        db.insert("EVENT", null, eventValues);

    }

    protected static void insertSong(SQLiteDatabase db,
                                     String name, String link) {

        ContentValues songValues = new ContentValues();
        songValues.put("SONGNAME", name);
        songValues.put("LINK", link);
        db.insert("SONGS", null, songValues);

    }

    protected static void insertVideo(SQLiteDatabase db,
                                     String name, String link) {

        ContentValues videoValues = new ContentValues();
        videoValues.put("SONGNAME", name);
        videoValues.put("LINK", link);
        db.insert("VIDEOS", null, videoValues);

    }

    protected static void insertRead(SQLiteDatabase db,
                                      String article, String link) {

        ContentValues videoValues = new ContentValues();
        videoValues.put("ARTICLESNAMES", article);
        videoValues.put("LINK", link);
        db.insert("ARTICLES", null, videoValues);

    }
    protected static void insertHistorical(SQLiteDatabase db,
                                           String name, int date, String desc,
                                           int resourceId, String address) {

        ContentValues eventValues = new ContentValues();
        eventValues.put("NAME", name);
        eventValues.put("IMAGE_RESOURCE_ID", resourceId);
        eventValues.put("SINCE", date );
        eventValues.put("Description", desc);
        eventValues.put("ADDRESS", address );
        db.insert("HISTORICAL", null, eventValues);

    }
    protected static void insertEntertainment(SQLiteDatabase db,
                                              String name, String type, String link,
                                              int resourceId) {

        ContentValues eventValues = new ContentValues();
        eventValues.put("NAME", name);
        eventValues.put("IMAGE_RESOURCE_ID", resourceId);
        eventValues.put("TYPE", type);
        eventValues.put("LINK", link);
        db.insert("ENTERTAINMENT", null, eventValues);

    }


    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if (oldVersion < 1) {
            db.execSQL("CREATE TABLE EVENT (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "IMAGE_RESOURCE_ID INTEGER,"
                    + "DATE_TIME TEXT,"
                    + "DESCRIPTION TEXT,"
                    + "ADDRESS TEXT);");

            db.execSQL("CREATE TABLE HISTORICAL (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "IMAGE_RESOURCE_ID INTEGER,"
                    + "SINCE INTEGER,"
                    + "DESCRIPTION TEXT, "
                    + "ADDRESS TEXT);");

            db.execSQL("CREATE TABLE ENTERTAINMENT (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "NAME TEXT, "
                    + "IMAGE_RESOURCE_ID INTEGER,"
                    + "LINK TEXT,"
                    + "TYPE TEXT);");

            db.execSQL("CREATE TABLE SONGS (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "SONGNAME TEXT, "
                    + "LINK TEXT);");

            db.execSQL("CREATE TABLE VIDEOS (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "SONGNAME TEXT, "
                    + "LINK TEXT);");

            db.execSQL("CREATE TABLE ARTICLES (_id INTEGER PRIMARY KEY AUTOINCREMENT, "
                    + "ARTICLESNAMES TEXT, "
                    + "LINK TEXT);");

        }

    }
}
