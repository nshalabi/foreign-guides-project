package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.recyclerview.widget.LinearLayoutManager;
import androidx.recyclerview.widget.RecyclerView;

import android.content.ContentValues;
import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.finalproject.myDatabase.insertEvent;

public class events extends AppCompatActivity {
    ArrayList<String> names = new ArrayList<>();
    ArrayList<String> dates = new ArrayList<>();
    ArrayList<String> description = new ArrayList<>();
    ArrayList<Integer> imgId = new ArrayList<>();
    ArrayList<String> addresses = new ArrayList<>();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_events);
        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);
        RecyclerView recycler = (RecyclerView) findViewById(R.id.events_recycler);

        SQLiteOpenHelper databaseHelper = new myDatabase(this);
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
            // insertEvent(db, "The Amazing Acro", "Fri, Dec 6, 2019, 7:00 PM", "Presented by Rock Cats Rescue and partnered with Jefferson SPCA. A portion of the proceeds will be donated to their cause, all of the proceeds benefit cat and kitten rescue! The AllWays Lounge & Cabaret 2240 Saint Claude Avenue.New Orleans, LA 70117", R.drawable.acro );
            //insertEvent(db, "Coding bootcamp", "December 9, 2019. 7:30 PM – 9:30 PM", "This is a 16 hours long Programming course for beginners taught over 5 weeks which introduces students to computer programming", R.drawable.bootcamp);
            //insertEvent(db, "LUNA Lounge", "Thu, Dec 12, 2019, 6:00 PM", "LUNA Fête is a visionary initiative by the Arts Council New Orleans. We utilize local iconic architecture and contemporary light, sound installation, motion graphics, and video-mapping practices to create a series of artistic large-scale outdoor light installations across the city each December.", R.drawable.lina2, "2019400 Lafayette Street. New Orleans, Louisiana 70130");
           // insertEvent(db, "XXXmas Cut Up in The Penthouse at The Maison", "Sat, Dec 14, 2019, 10:00 PM", "21+ Free Entry for ladies all night!The Maison Penthouse (Third Floor)", R.drawable.xxxmas1, "508 Frenchmen St. New Orleans, LA 70116");
            //insertEvent(db, " New Years Eve Extravaganza with OOKAY.", "Tue, December 31, 2019, 9:00 PM ","THREE ROOMS OF MUSIC-BALLOON DROP--CHAMPAGNE TOAST AT MIDNIGHT--ALL-INCLUSIVE VIP TICKETS AVAILABLE --VIP SECTIONS AVAILABLE. AGES: 18+ ONLY.", R.drawable.oookay1, "310 Andrew Higgins Blvd. New Orleans, LA 70130" );
            //insertEvent(db, "Barriere Holiday Party.", "TBA", "Cocktail Reception Attire/ Please RSVP by December 2nd/ Parking is directly across the street from The Cannery./ We hope you can make it!. Cheers, Barriere", R.drawable.barrieparty1, " null");
            Cursor cursor = db.query("EVENT", new String[]{"_id", "NAME", "DATE_TIME", "DESCRIPTION", "IMAGE_RESOURCE_ID", "ADDRESS"}, null, null, null, null, null);

            if(cursor.moveToFirst()) {

                do {
                    names.add(cursor.getString(1));
                    dates.add(cursor.getString(2));
                    description.add(cursor.getString(3));
                    imgId.add(cursor.getInt(4));
                    addresses.add(cursor.getString(5));
                } while (cursor.moveToNext());
            }
            else{
                Log.d("test", "apparantly there wasn't a 'first' to move the cursor to!");
            }

            cursor.close();
            db.close();
        } catch(SQLiteException e){
            Toast toast = Toast.makeText(this, "database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }//end of catch block

        RecyclerAdapter adapter = new RecyclerAdapter(names, imgId, description, dates);
        recycler.setAdapter(adapter);
        recycler.setLayoutManager( new LinearLayoutManager(this));

        adapter.setListener(new RecyclerAdapter.Listener() {
            @Override
            public void onClick(int position, View view) {

                String location = addresses.get(position);
                if(!location.equals("NULL") && !location.equals(" null")) {
                    String map = "http://maps.google.co.in/maps?q=" + location;
                    Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
                    startActivity(intent);
                }else{
                    Toast toast = Toast.makeText(view.getContext(), "unavailable address right now", Toast.LENGTH_SHORT);
                    toast.show();
                }

            }
        });
    }//end of onCreate method
}//end of class Events

