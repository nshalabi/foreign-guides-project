package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;
import androidx.core.content.ContextCompat;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.graphics.drawable.Drawable;
import android.graphics.drawable.Icon;
import android.net.Uri;
import android.os.Bundle;
import android.text.SpannableString;
import android.text.style.UnderlineSpan;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

public class HotelDetailActivity extends AppCompatActivity {

    public static final String HOTEL_ID = "hotelID";
    private String website = "";
    private String address = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_hotel_detail);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        int hotelId = (Integer) getIntent().getExtras().getInt(HOTEL_ID);

        SQLiteOpenHelper dbHelper = new DBHelper(this);

        try {
            SQLiteDatabase readDB = dbHelper.getReadableDatabase();
            Cursor cursor = readDB.query("HOTEL",
                new String[]{"_id", "NAME", "OTHER_IMAGE_ID", "ADDRESS",
                        "WEBSITE", "STARS"},
                    "_id = ?",
                    new String[]{Integer.toString(hotelId+1)},
                    null, null, null);

            if(cursor.moveToFirst()) {

                TextView name = (TextView) findViewById(R.id.hotelName);
                name.setText(cursor.getString(1));

                ImageView img = (ImageView) findViewById(R.id.hotelOtherImg);
                img.setImageResource(cursor.getInt(2));
                img.setContentDescription(cursor.getString(1));

                TextView addr = (TextView) findViewById(R.id.hotelAddr);
                addr.setText(cursor.getString(3));
                address = cursor.getString(3).toString();

                TextView web = (TextView) findViewById(R.id.hotelWeb);
                SpannableString content = new SpannableString("Go to: "+ cursor.getString(4).toString());
                content.setSpan(new UnderlineSpan(), 7, content.length(), 0);
                web.setText(content);
                //web.setText(cursor.getString(4));
                website = cursor.getString(4).toString();

                int nbOfstars = cursor.getInt(5);

                TextView stars = (TextView) findViewById(R.id.hotelStars);
                stars.setText(nbOfstars+" stars");

                ImageView starsImg = (ImageView) findViewById(R.id.hotelStarsImg);

                switch (nbOfstars) {
                    case 0:
                        starsImg.setImageResource(R.drawable.zerostars);
                        break;
                    case 1:
                        starsImg.setImageResource(R.drawable.onestars);
                        break;
                    case 2:
                        starsImg.setImageResource(R.drawable.twostars);
                        break;
                    case 3:
                        starsImg.setImageResource(R.drawable.threestars);
                        break;
                    case 4:
                        starsImg.setImageResource(R.drawable.fourstars);
                        break;
                    case 5:
                        starsImg.setImageResource(R.drawable.fivestars);
                        break;
                }
                starsImg.setContentDescription(stars.getText().toString());

            }

            cursor.close();
            readDB.close();
        } catch (SQLiteException e) {
            Toast toast = Toast.makeText(this,
                "Database Unavailable!", Toast.LENGTH_LONG);
            toast.show();
        }
    }

    public void onHotelWebClick(View v) {
        Intent webIntent = new Intent(Intent.ACTION_VIEW, Uri.parse("http://"+website));
        startActivity(webIntent);
    }

    public void onHotelAddrClick(View v) {
        String map = "http://maps.google.co.in/maps?q=" + address;
        Intent intent = new Intent(Intent.ACTION_VIEW, Uri.parse(map));
        startActivity(intent);
    }
}
