package com.example.finalproject;

import android.content.ContentValues;
import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import java.sql.Date;

public class DBHelper extends SQLiteOpenHelper {

    private static String DB_NAME="database2";
    private static int DB_VERSION=1;

    public DBHelper(Context context) {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onCreate(SQLiteDatabase db) {
        updateMyDatabase(db, 0, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
        updateMyDatabase(db, oldVersion, newVersion);
    }

    private void updateMyDatabase(SQLiteDatabase db, int oldVersion, int newVersion) {
        if(oldVersion < 1) {
            db.execSQL("CREATE TABLE PERSON (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "USERNAME TEXT, PASSWORD TEXT, " +
                    "FIRSTNAME TEXT, LASTNAME TEXT, EMAIL TEXT, DATEOFBIRTH TEXT);"); //yyyy-mm-dd

            db.execSQL("CREATE TABLE HOTEL (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "NAME TEXT, " +
                    "IMAGE_ID INTEGER, " +
                    "OTHER_IMAGE_ID INTEGER, " +
                    "ADDRESS TEXT, " +
                    "WEBSITE TEXT, " +
                    "STARS INTEGER);");

            db.execSQL("CREATE TABLE RESTAURANT (_id INTEGER PRIMARY KEY AUTOINCREMENT, " +
                    "NAME TEXT, " +
                    "IMAGE_ID INTEGER, " +
                    "DISH_IMAGE_ID INTEGER, " +
                    "ADDRESS TEXT, " +
                    "WEBSITE TEXT, " +
                    "DISH_NAME TEXT, " +
                    "DISH_DESC TEXT);");

            insertHotel(db, "Hilton New Orleans Riverside",
                    R.drawable.hiltonmain, R.drawable.hiltonother,
                    "2 Poydras St, New Orleans, LA 70130",
                    "www.hilton.com", 4);

            insertHotel(db, "Omni Riverfront Hotel",
                    R.drawable.omnimain, R.drawable.omniother,
                    "701 Convention Center Blvd, New Orleans, LA 70130",
                    "www.omnihotels.com", 4);

//            insertHotel(db, "Windsor Court Hotel",
//                    R.drawable.windsormain, R.drawable.windsorother,
//                    "300 Gravier St, New Orleans, LA 70130",
//                    "www.windsorcourthotel.com", 5);
//
//            insertHotel(db, "New Orleans Marriott",
//                    R.drawable.marriottmain, R.drawable.marriottother,
//                    "555 Canal St, New Orleans, LA 70130",
//                    "www.marriott.com", 4);
//
//            insertHotel(db, "Le Pavillon Hotel",
//                    R.drawable.pavillonmain, R.drawable.pavillonother,
//                    "833 Poydras St, New Orleans, LA 70112",
//                    "www.lepavillon.com", 4);
//
//            insertHotel(db, "The Pontchartrain Hotel",
//                    R.drawable.pontchartrainmain, R.drawable.pontchartrainother,
//                    "2031 St Charles Ave, New Orleans, LA 70130",
//                    "www.thepontchartrainhotel.com", 3);


            insertRestaurant(db, "Cafe du Monde", R.drawable.cafedumonde,
                    R.drawable.beignets, "800 Decatur St, New Orleans, LA 70116",
                    "www.cafedumonde.com", "Beignets",
                    "Square piece of dough, fried and covered with powdered sugar");

            insertRestaurant(db, "Galatoire's", R.drawable.galatoires,
                    R.drawable.crabmaison, "209 Bourbon St, New Orleans, LA 70130",
                    "www.galatoires.com", "Crab Maison",
                    "Louisiana jumbo lump crab, green onions, capers, creole mustard aioli");
        }
    }

    private static void insertRestaurant(SQLiteDatabase db, String name, int imgID, int dishImgID,
                                    String address, String website, String dishName, String dishDesc) {
        ContentValues restaurant = new ContentValues();
        restaurant.put("NAME", name);
        restaurant.put("IMAGE_ID", imgID);
        restaurant.put("DISH_IMAGE_ID", dishImgID);
        restaurant.put("ADDRESS", address);
        restaurant.put("WEBSITE", website);
        restaurant.put("DISH_NAME", dishName);
        restaurant.put("DISH_DESC", dishDesc);

        db.insert("RESTAURANT", null, restaurant);
    }

    private static void insertHotel(SQLiteDatabase db, String name, int mainImgID, int otherImgID,
                                    String address, String website, int stars) {
        ContentValues hotel = new ContentValues();
        hotel.put("NAME", name);
        hotel.put("IMAGE_ID", mainImgID);
        hotel.put("OTHER_IMAGE_ID", otherImgID);
        hotel.put("ADDRESS", address);
        hotel.put("WEBSITE", website);
        hotel.put("STARS", stars);

        db.insert("HOTEL", null, hotel);
    }

    private static void insertPerson(SQLiteDatabase db, String user, String pass,
                                     String firstName, String lastName, String email,
                                     String dateOfBirth) {
        ContentValues personValues = new ContentValues();
        personValues.put("USERNAME", user);
        personValues.put("PASSWORD", pass);
        personValues.put("FIRSTNAME", firstName);
        personValues.put("LASTNAME", lastName);
        personValues.put("EMAIL", email);
        personValues.put("DATEOFBIRTH", dateOfBirth);

        db.insert("PERSON", null, personValues);
    }
}
