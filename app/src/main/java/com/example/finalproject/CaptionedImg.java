package com.example.finalproject;

import android.graphics.drawable.Drawable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.cardview.widget.CardView;
import androidx.core.content.ContextCompat;
import androidx.recyclerview.widget.RecyclerView;

public class CaptionedImg extends RecyclerView.Adapter<CaptionedImg.ViewHolder> {

    interface Listener {
        void onClick(int position);
    }

    private String[] captions;
    private int[] imgIDs;
    private Listener listener;

    public CaptionedImg(String[] captions, int[] imgIDs) {
        this.captions = captions;
        this.imgIDs = imgIDs;
    }

    public void setListener(Listener listener) {
        this.listener = listener;
    }

    @Override
    public CaptionedImg.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        CardView cv = (CardView) LayoutInflater.from(parent.getContext()).
                inflate(R.layout.captioned_image, parent, false);

        return new ViewHolder(cv);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        CardView cardView = holder.cardView;
        ImageView mainImg = (ImageView) cardView.findViewById(R.id.cardImg);
        Drawable drawable = ContextCompat.getDrawable(cardView.getContext(), imgIDs[position]);
        mainImg.setImageDrawable(drawable);
        mainImg.setContentDescription(captions[position]);

        TextView txt = (TextView) cardView.findViewById(R.id.cardTxt);
        txt.setText(captions[position]);

        cardView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if(listener != null) {
                    listener.onClick(position);
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return captions.length;
    }

    public static class ViewHolder extends RecyclerView.ViewHolder {

        private CardView cardView;

        public ViewHolder(CardView cardView) {
            super(cardView);
            this.cardView = cardView;
        }
    }
}
