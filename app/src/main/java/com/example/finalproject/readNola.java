package com.example.finalproject;

import androidx.appcompat.app.ActionBar;
import androidx.appcompat.app.AppCompatActivity;
import androidx.appcompat.widget.Toolbar;

import android.content.Intent;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteException;
import android.database.sqlite.SQLiteOpenHelper;
import android.net.Uri;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.AdapterView;
import android.widget.ArrayAdapter;
import android.widget.ListView;
import android.widget.Toast;

import java.util.ArrayList;

import static com.example.finalproject.myDatabase.insertRead;

public class readNola extends AppCompatActivity {

    private ListView lv;
    ArrayList<String> articlesNames;
    ArrayList<String> articlesLinks;
    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_read_nola);

        Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);
        ActionBar actionBar = getSupportActionBar();
        actionBar.setDisplayHomeAsUpEnabled(true);

        lv = (ListView) findViewById(R.id.lv);

        articlesNames = new ArrayList<>();
        articlesLinks = new ArrayList<>();

        SQLiteOpenHelper databaseHelper = new myDatabase(this);
        try{
            SQLiteDatabase db = databaseHelper.getWritableDatabase();
//            insertRead(db,"New OrLEANS, LOUISIANA, united states", "https://www.britannica.com/place/New-Orleans-Louisiana" );
//            insertRead(db,"New Orleans Beyond Bourbon Street", "https://www.smithsonianmag.com/travel/new-orleans-beyond-bourbon-street-48566775/" );
//            insertRead(db,"New Orleans Travel Guides", "https://www.travelandleisure.com/travel-guide/new-orleans" );
//            insertRead(db,"New Orleans Related Articles", "https://www.travelchannel.com/destinations/us/la/new-orleans/articles/explore-new-orleans-beyond-the-french-quarter" );
//            insertRead(db,"New Orleans Beyond the Festival", "https://www.travelchannel.com/destinations/us/la/new-orleans/articles/new-orleans-beyond-the-festival" );
//            insertRead(db,"New Orleans History", "https://www.history.com/topics/us-states/new-orleans#section_1" );


            Cursor cursor = db.query("ARTICLES", new String[]{"_id", "ARTICLESNAMES", "LINK"}, null, null, null, null, null);
            if(cursor.moveToFirst()) {

                do {
                    articlesNames.add(cursor.getString(1));
                    articlesLinks.add(cursor.getString(2));


                } while (cursor.moveToNext());
            }
            else{
                Log.d("test", "apparantly there wasn't a 'first' to move the cursor to!");
            }

            cursor.close();
            db.close();
        } catch(SQLiteException e){
            Toast toast = Toast.makeText(this, "database unavailable", Toast.LENGTH_SHORT);
            toast.show();
        }//end of catch block

        // This is the array adapter, it takes the context of the activity as a
        // first parameter, the type of list view as a second parameter and your
        // array as a third parameter.
        ArrayAdapter<String> arrayAdapter = new ArrayAdapter<>(
                this,
                android.R.layout.simple_expandable_list_item_1,
                articlesNames );

        lv.setAdapter(arrayAdapter);
        lv.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            public void onItemClick(AdapterView<?> arg0, View arg1, int position, long arg3)
            {
                Intent myIntent = new Intent(Intent.ACTION_VIEW, Uri.parse(articlesLinks.get(position)));
                startActivity(myIntent);

            }
        });

    }
}
